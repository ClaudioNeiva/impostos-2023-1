package br.ucsal.testequalidade.impostos.business;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class InssBO {

	private static final BigDecimal ALIQUOTA_1 = new BigDecimal("0.075");
	private static final BigDecimal ALIQUOTA_2 = new BigDecimal("0.090");
	private static final BigDecimal ALIQUOTA_3 = new BigDecimal("0.120");
	private static final BigDecimal ALIQUOTA_4 = new BigDecimal("0.140");
	private static final BigDecimal FAIXA_1_TOPO = new BigDecimal("1302.00");
	private static final BigDecimal FAIXA_2_TOPO = new BigDecimal("2571.29");
	private static final BigDecimal FAIXA_3_TOPO = new BigDecimal("3856.94");
	private static final BigDecimal FAIXA_4_TOPO = new BigDecimal("7507.49");
	private static final Integer QTD_CASAS_DECIMAIS = 2;
	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;

	private InssBO() {
	}

	public static BigDecimal calcularInss(BigDecimal salarioBruto) {
		BigDecimal inss = new BigDecimal("0.00");
		BigDecimal baseCalculo = salarioBruto;
		BigDecimal baseCalculoFaixa;
		BigDecimal inssFaixa;

		if (baseCalculo.compareTo(FAIXA_4_TOPO) > 0) {
			baseCalculo = FAIXA_4_TOPO;
		}
		
		if (baseCalculo.compareTo(FAIXA_3_TOPO) > 0) {
			baseCalculoFaixa = baseCalculo.subtract(FAIXA_3_TOPO);
			inssFaixa = baseCalculoFaixa.multiply(ALIQUOTA_4);
			inss = inss.add(inssFaixa);
			baseCalculo = FAIXA_3_TOPO;
		}
		if (baseCalculo.compareTo(FAIXA_2_TOPO) > 0) {
			baseCalculoFaixa = baseCalculo.subtract(FAIXA_2_TOPO);
			inssFaixa = baseCalculoFaixa.multiply(ALIQUOTA_3);
			inss = inss.add(inssFaixa);
			baseCalculo = FAIXA_2_TOPO;
		}
		if (baseCalculo.compareTo(FAIXA_1_TOPO) > 0) {
			baseCalculoFaixa = baseCalculo.subtract(FAIXA_1_TOPO);
			inssFaixa = baseCalculoFaixa.multiply(ALIQUOTA_2);
			inss = inss.add(inssFaixa);
			baseCalculo = FAIXA_1_TOPO;
		}

		baseCalculoFaixa = baseCalculo;
		inssFaixa = baseCalculoFaixa.multiply(ALIQUOTA_1);
		inss = inss.add(inssFaixa);
		
		return inss.setScale(QTD_CASAS_DECIMAIS, ROUNDING_MODE);
	}

}
